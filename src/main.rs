use clap::Parser;
use glob::glob;
use lofty::{AudioFile, Probe};
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::fs;
use std::path::PathBuf;
use uuid::Uuid;

const NETMD_PADDING_IN_SECONDS: u64 = 2;

#[derive(Parser, Debug)]
struct Args {
    input_dir: PathBuf,
    output_dir: PathBuf,
    #[arg(short, long, default_value_t = 74, help = "in number of minutes")]
    disc_size: u64,
    #[arg(
        short,
        long,
        default_value_t = 4,
        help = "1, 2, 4 with 1 being sp and 4 being lp4"
    )]
    compression: u64,
}

struct Track {
    id: Uuid,
    duration_in_secs: u64,
    path: PathBuf,
}

fn get_track_paths(root: &PathBuf) -> Result<Vec<PathBuf>, std::io::Error> {
    let mut track_paths = Vec::<PathBuf>::new();

    // TODO: support other music formats "{mp3,flac}" doesn't work
    if let Ok(glob_pattern) = root.join("**/*.mp3").into_os_string().into_string() {
        for entry in glob(&glob_pattern).expect("Failed to read glob pattern") {
            if let Ok(path) = entry {
                track_paths.push(path);
            }
        }
    };

    Ok(track_paths)
}

fn read_track(path: PathBuf) -> Track {
    let tagged_file = Probe::open(&path)
        .expect("ERROR: Bad path provided!")
        .read()
        .expect("ERROR: Failed to read file!");

    let properties = tagged_file.properties();

    let duration = properties.duration();

    Track {
        id: Uuid::new_v4(),
        path: path.to_path_buf(),
        duration_in_secs: duration.as_secs(),
    }
}

fn main() {
    let args = Args::parse();

    // Read the input dir for files.
    let track_paths = get_track_paths(&args.input_dir).unwrap_or(vec![]);

    println!("found files: {:?}", track_paths.len());

    println!("getting track durations...");

    // Get their runtimes and names.
    let tracks = track_paths
        .into_iter()
        .map(read_track)
        .collect::<Vec<Track>>();

    println!("generating a playlist...");

    let mut rng = thread_rng();

    let mut playlist = Vec::<PathBuf>::new();

    let mut chosen_tracks = Vec::<Uuid>::new();

    let mut avaliable_seconds = args.disc_size * 60 * args.compression;

    let mut retries = 10;

    println!("avaliable seconds: {:?}", avaliable_seconds);

    // Select a group of tracks that fit the size.
    while avaliable_seconds > 30 {
        match tracks.choose(&mut rng) {
            Some(track) => {
                if retries == 0 {
                    println!("ran out of retries");
                    break;
                } else if track.duration_in_secs > avaliable_seconds {
                    println!("track too large, skipping");
                    retries = retries - 1;
                } else if chosen_tracks.contains(&track.id) {
                    println!("track already in playlist, skipping");
                } else {
                    playlist.push(track.path.clone());
                    chosen_tracks.push(track.id);
                    avaliable_seconds =
                        avaliable_seconds - track.duration_in_secs - NETMD_PADDING_IN_SECONDS;

                    println!("seconds left to fill: {:?}", avaliable_seconds);
                }
            }
            None => {
                println!("rng failed");
                continue;
            }
        }
    }

    println!("copying playlist tracks to out dir...");

    // Create the output directory if it doesn't exist.
    fs::create_dir_all(&args.output_dir).expect("failed to create output directory");

    // Copy those group of files to the new directory.
    for track in playlist {
        let filename = track
            .file_name()
            .expect("some how file didn't have a file name");

        let new_path = args.output_dir.join(filename);

        match fs::copy(&track, &new_path) {
            Ok(_) => println!("copied {:?} to {:?}", filename, new_path),
            Err(_) => panic!(),
        }
    }
}
